from unittest import TestCase
from Task1 import task1

test_data_x = [
    [["ab", "bc", "a"], "abc"],
    [["a", "ab", "bc"], "ab"],
    [["a", "ab", "bc"], ""],
    [["ab", "bc"], "abc"],
    [["ab", "bc", "c"], "abbcbc"],
    [[], "asd"],
    [["ab", "c", "bca", "dc", "cac"], "abcbcacabdcabcabccaccacdcabcacac"],
    [["ab", "c", "bca", "dc", "cac"], "abccacabdcabcabccaccacdcabcacc"]

]
test_data_y = [
    True,
    True,
    True,
    False,
    True,
    False,
    False,
    True
]


class TestTask1(TestCase):
    def test_task1_results(self):
        for x, y in zip(test_data_x, test_data_y):
            print(x, y)
            x = task1(x[0], x[1])
            print(x)
            self.assertEqual(y, x)

    def test_task1_input_check(self):
        with self.assertRaises(TypeError):
            task1(['3', 234, True], "asdasd")
        with self.assertRaises(TypeError):
            task1(['d', "sda", "asdd"], True)
        with self.assertRaises(TypeError):
            task1("asdasdasd", "asdasdasdasdasdas")
