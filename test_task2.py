from unittest import TestCase
from Task2 import task2

test_data_x = [
    [[1, 3], [3, 4], [9, 10], [-10, 0], [-20, -1]],
    [[-10, 0], [3, 4], [1, 3], [9, 10], [-20, -1]],
    [[1, 3], [1, 4], [9, 14], [10, 12], [-20, -1]],
    [[-10, -3], [-3, -1], [2, 10], [1, 3], [-2, -1]],
    [[-20, -1], [-1, 0], [0, 2], [3, 10], [2, 9]],
    []

]
test_data_y = [
    [[1, 4], [9, 10], [-20, 0]],
    [[1, 4], [9, 10], [-20, 0]],
    [[1, 4], [9, 14], [-20, -1]],
    [[-10, -1], [1, 10]],
    [[-20, 10]],
    []
]


class TestTask2(TestCase):
    def test_task2_results(self):
        for x, y in zip(test_data_x, test_data_y):
            x = task2(x)
            x.sort()
            y.sort()
            self.assertEqual(y, x)

    def test_task2_input_check(self):
        with self.assertRaises(TypeError):
            task2([[1 / 2, 1], [0, 2]])
        with self.assertRaises(TypeError):
            task2([[1], [0, 2]])
        with self.assertRaises(TypeError):
            task2([[0, 1, 3], [0, 2]])
        with self.assertRaises(TypeError):
            task2([[0, "1"], [0, 2]])
        with self.assertRaises(TypeError):
            task2(123)
