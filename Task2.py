import math


def isnumber(num):
    return isinstance(num, int)



def task2(intervals: list):
    if not isinstance(intervals, list) or not all([len(i) == 2 and isnumber(i[0]) and isnumber(i[1]) for i in intervals]):
        raise TypeError("Input should be a list of intervals (two integer points) !!!")

    intervals.sort(key=lambda a: a[0])
    for i in range(len(intervals) - 1):
        if (intervals[i][0] <= intervals[i + 1][0] <= intervals[i][1]) or (intervals[i + 1][0] <= intervals[i][0] <= intervals[i + 1][1]):
            intervals[i + 1] = [min(intervals[i][0], intervals[i + 1][0]), max(intervals[i][1], intervals[i + 1][1])]
            intervals[i] = [math.inf, math.inf]
    return list(filter(lambda xc: xc != [math.inf, math.inf], intervals))
