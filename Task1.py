def task1_v1(tokens: list, text: str, last_check: list = None):
    if not isinstance(text, str):
        raise TypeError("text musb be a string not a " + str(type(text)) + "!!!")
    if not isinstance(tokens, list) or (not any([isinstance(t, str) for t in tokens]) and len(tokens) > 0):
        raise TypeError("Function accept only a list of string as tokens !!!")

    if last_check is None:
        last_check = []
    if len(text) == 0:
        return True

    flag = False
    for token in tokens:
        if token in text:
            flag = True
    if not flag:
        return False

    actual_procesed_text = ""
    already_ok_fragments = []
    for char in text:
        actual_procesed_text += char
        for token in tokens:
            if actual_procesed_text == token:
                already_ok_fragments.append(actual_procesed_text)
                actual_procesed_text = ""
                break

    if actual_procesed_text != "" and len(last_check) > 0:
        temp_actual_procesed_text = actual_procesed_text
        actual_procesed_text = ""
        for char in temp_actual_procesed_text:
            actual_procesed_text += char
            for token in last_check:
                if actual_procesed_text == token:
                    already_ok_fragments.append(actual_procesed_text)
                    actual_procesed_text = ""
                    break
            for token in tokens:
                if actual_procesed_text == token:
                    already_ok_fragments.append(actual_procesed_text)
                    actual_procesed_text = ""
                    break

    if actual_procesed_text == "":
        return True
    if len(already_ok_fragments) == 0:
        return False

    temp = already_ok_fragments[-1]
    actual_procesed_text = temp + actual_procesed_text
    if temp in tokens:
        del tokens[tokens.index(temp)]
    else:
        if len(last_check) > 0 and last_check[-1] == temp and text == actual_procesed_text:
            return False
        del last_check[last_check.index(temp)]

    last_check.append(temp)

    return task1_v1(tokens, actual_procesed_text, last_check)


def task1(tokens, text, precrocess=True):
    if not isinstance(text, str):
        raise TypeError("text musb be a string not a " + str(type(text)) + "!!!")
    if not isinstance(tokens, list) or (not any([isinstance(t, str) for t in tokens]) and len(tokens) > 0):
        raise TypeError("Function accept only a list of string as tokens !!!")

    if len(text) == 0:
        return True

    if precrocess:
        if len(tokens) == 0:
            return False
        tokens = list(filter(lambda x: x in text, tokens))  # remove redundant tokens
        tokens = list(dict.fromkeys(tokens))  # remove duplicates
        tokens.sort(key=len, reverse=False)  # sort by length
        print(tokens)

        print(text)
    for i in range(1, len(text) + 1):
        print(text[:i], text[:i] in tokens)
        if text[:i] in tokens and task1(tokens, text[i:], False):
            return True
    return False
