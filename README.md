# Luxoft recruitment tasks

## Tasks

### Task1

1.Write a function/method that accepts a list of string tokens and a separate text string as an input and checks if the latter string can be represented as a concatenation of any subset of tokens from the first argument, where each token can be used multiple times. Examples:
```
    [ "ab", "bc", "a" ] vs. "abc" = true
    [ "a", "ab", "bc" ] vs. "ab" = true
    [ "a", "ab", "bc" ] vs. "" = true
    [ "ab", "bc" ] <-> "abc" = false
    [ "ab", "bc", "c" ] <-> "abbcbc" = true
```
My answer:
```
function task1_v1 in Task1 file
```
2.Estimate the computational complexity of your code.
My answer:
```
O((3n^2*k^2)/2) in worst case
between O(n) or O(n*k) (it depends on tokens order) in best scenario
where n is a text lenght and k is a count of tokens
```
3.Let's assume the list of tokens can be preprocessed somehow once, and then the result is to be multiple times applied to long input strings. E.g. in Golang notation:
```golang
func preprocess(tokens []string) func(input string) bool {
...
}

validator := preprocess(someTokens)
for _, input := range longListOfInputs {
fmt.Printf("%s: %v\n", input, validator(input))
}
```
We don't care about preprocessor() performance much, but we want validator() to run as far as possible. Could you suggest any implementation that satisfies this extra requirement? if so, what's the computational complexity of that implementation?
My answer:
```
function task1 in Task1.py file
its not change computational complexity formula as much but it can reduce k by removing redundant tokens and dublicates.Moreover sorting tokens by lenght can help becouse we can break the loop if token lenght is greater than actual_procesed_text lenght
```
### Task2

1.The input is a list of intervals (two integer points); write a function/method that merges any N intervals in the list that have common points (intervals [1, 3] and [3, 6] have a common point of 3; [4, 8] and [6, 10] have common points of 6, 7, 8) and returns the merged list (where no two intervals intersect). it is allowed to modify the input. Try to avoid allocating extra memory for the output.
My answer:
```
function task2 in Task2 file
```
2.What is the computational complexity of your implementation?
My answer:
```
O(n-1) in best scenario
O(1+2+3+...+(n-1)) in worst scenario
where n is a input list lenght
```

### Other Assumptions
The tasks are to be implemented in any programming language, though we'd give bonus points if it would be Golang (as our primary backend language). In case some more esoteric programming language is chosen, please make sure the code is structured and documented in the way a person that does not "speak" that language could still understand the flow. The tasks can be passed back in an email as attachments, in some archive or put in some code sharing service / online repository (with proper permissions so we can access it). The answers to the questions about the complexity could be put into the code as comments.

## License
All rights to the content of the tasks belong to the [Luxoft](https://www.luxoft.com/).